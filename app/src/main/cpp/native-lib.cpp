#include <jni.h>
#include <string>
#include <syslog.h>


extern "C" JNIEXPORT jstring

JNICALL
Java_com_budstudio_lc32xwifi_nativeCls_nativeLib_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";

    return env->NewStringUTF(hello.c_str());
}
